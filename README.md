# Toolforge sample complex application - frontend

This is a sample application to measure and demonstrate different features of
the toolforge platform.

It's meant to be rather complex so it uses many features at the same time, for
simpler and more-specific examples you can
[https://toolhub.wikimedia.org/search?q=sample-&ordering=-score&page=1&page_size=12](search
for 'sample-\*' tools on toolhub).

# Development environment

Install the dependencies:

```
> npm install
```

Then run the serve command to start a development server:

```
> npm run dev
```

## Production build test

Note that in production, we start a nodejs serve (`server.js`) that then serves
the vuejs application static files built under `dist`.

```
> npm run build
> node server.js
```

# Building in toolforge

Ssh to the bastion and run:

```
mylaptop> ssh login.toolforge.org
myuser@toolforge> become sample-complex-app
sample-complex-app@toolforge> toolforge build start --image-name frontend https://gitlab.wikimedia.org/toolforge-repos/sample-complex-app-frontend
```

# Running in toolforge

You will need to configure the backend url, using an envvar:

```
sample-complex-app@toolforge> toolforge envvar create BACKEND_URL "http://backend-api:8000"
```

Just run it as a webservice:

```
sample-complex-app@toolforge> toolforge webservice buildservice start --buildservice-image tool-sample-complex-app/frontend --mount=none
```
